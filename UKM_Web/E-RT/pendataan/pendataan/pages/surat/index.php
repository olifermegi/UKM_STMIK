<?php include('../_partials/top.php') ?>
<?php include('datai-index.php');  ?>
<h1 class="page-header">Pembuatan surat pengantar</h1>
<form method="post">
<!-- <h3>Pembuatan Surat Pengantar</h3> -->
<table class="table table-striped table-middle">
  <tr>
    <th width="20%">Nama Lengkap</th>
    <td width="1%">:</td>
    <td> 
      <select class="form-control selectpicker" name="nama" id="nama" onchange="changeValue(this.value)">
      <option value="" selected disabled>- pilih -</option>
     <?php 
    $results = mysql_query("select * from warga");    
    $jsArray = "var dtwrg = new Array();\n";        
    while ($row = mysql_fetch_array($results)) {    
        echo '<option value="' . $row['nama_warga'] . '">' . $row['nama_warga'] . '</option>';    
        $jsArray .= "dtwrg['" . $row['nama_warga'] . "'] = {pkrjn:'" . addslashes($row['pekerjaan_warga']) . "',nik:'".addslashes($row['nik_warga'])."'};\n";    
    }      
    ?>    
        </select>
    </td>
  </tr>
  <tr>
    <th>NIK</th>
    <td>:</td>
    <td><input type="text" class="form-control" name="nik" id="nik" required></td>
  </tr>
  <tr>
    <th>TTL</th>
    <td>:</td>
    <td><input type="text" class="form-control" name="ttl" required></td>
  </tr>
  <tr>
    <th>Pekerjaan</th>
    <td>:</td>
    <td><input type="pekerjaan" class="form-control" name="pekerjaan" id="pekerjaan"></td>
  </tr>
  <tr>
    <th>Agama</th>
    <td>:</td>
    <td>
      <select class="form-control selectpicker" name="status_user" required>
        <option value="" selected disabled>- pilih -</option>
        <option value="islam">Islam</option>
        <option value="kristen">Kristen</option>
        <option value="budha">Budha</option>
        <option value="hindu">Hindu</option>
      </select>
    </td>
    <tr>
    <tr>
    <th>Status</th>
    <td>:</td>
    <td>
      <select class="form-control selectpicker" name="status_user" required>
        <option value="" selected disabled>- pilih -</option>
        <option value="menikah">Menikah</option>
        <option value="janda">Janda</option>
        <option value="duda">Duda</option>
        <option value="lajang">Lajang</option>
      </select>
    </td>
    <tr>
    <tr>
    <th>Warga Negara</th>
    <td>:</td>
    <td>
      <select class="form-control selectpicker" name="warga_negara" required>
        <option value="" selected disabled>- pilih -</option>
        <option value="wni">WNI</option>
        <option value="wna">WNA</option>
      </select>
    </td>
   <tr>
   	<tr>
    	<th>Keperluan</th>
    	<td>:</td>
    	<td><textarea class="form-control" name="keperluan"></textarea></td>
  	</tr>
  </tr>
</table>

<h3>B. Data Alamat</h3>
<table class="table table-striped table-middle">
  <tr>
    <th width="20%">Desa/Kelurahan</th>
    <td width="1%">:</td>
    <td><input type="text" class="form-control" name="desa_kelurahan_user" required></td>
  </tr>
  <tr>
    <th>Kecamatan</th>
    <td>:</td>
    <td><input type="text" class="form-control" name="kecamatan_user" required></td>
  </tr>
  <tr>
    <th>Kabupaten/Kota</th>
    <td>:</td>
    <td><input type="text" class="form-control" name="kabupaten_kota_user" required></td>
  </tr>
  <tr>
    <th>Provinsi</th>
    <td>:</td>
    <td><input type="text" class="form-control" name="provinsi_user" required></td>
  </tr>
  <tr>
    <th>Negara</th>
    <td>:</td>
    <td><input type="text" class="form-control" name="negara_user" required></td>
  </tr>
  <tr>
    <th>RT</th>
    <td>:</td>
    <td><input type="text" class="form-control" name="rt_user" value="003"></td>
  </tr>
  <tr>
    <th>RW</th>
    <td>:</td>
    <td><input type="text" class="form-control" name="rw_user" value="016"></td>
  </tr>
</table>
 <script type="text/javascript">    
    <?php echo $jsArray; ?>  
    function changeValue(nama){  
    document.getElementById('pekerjaan').value = dtwrg[nama_warga].pekerjaan_warga;  
    document.getElementById('nik').value = dtwrg[nama_warga].nik_warga;  
    };  
    </script> 

<button type="submit" class="btn btn-primary btn-lg">
  <i class="glyphicon glyphicon-floppy-save"></i> Simpan
</button>
</form>

<?php include('../_partials/bottom.php') ?>
